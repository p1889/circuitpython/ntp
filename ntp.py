import time
import wifi
import rtc
import struct
import socketpool


class NTP:

    """ Helper class to get the RTC sync'd via NTP
    TODO: Add TZ ard to select TZ based on normalized locales
    """

    SECONDS_1970_TO_2000 = 946_684_800
    TZ_OFFSET = 0
    NTP_TIME_CORRECTION = 2_208_988_800
    NTP_SERVER = "pool.ntp.org"  # TODO: Handle array of NTP servers for fallback, with user supplied ones prepended
    NTP_PORT = 123

    SECONDS_TO_YEAR_2000 = time.mktime((2000,1,1,0,0,0,0,0,0))
    if SECONDS_TO_YEAR_2000 == 0:
        NTP_TIME_CORRECTION += SECONDS_1970_TO_2000 - SECONDS_TO_YEAR_2000

    def __init__(self, tz_offset=0, logger=None):
        """ Init our NTP instance
        Args:
            tz_offset (int): The number of hours to offset time by. Leaving this set to 
                0 will set the system clock to UTC
        """
        self.TZ_OFFSET = tz_offset
        self.logger = logger

    @classmethod
    def get_time(cls, pool=None):
        """ Gets and returns the current UTC time
        """

        # Raise an exception if we have no wifi connection
        if wifi.radio.ipv4_address is None:
            raise RuntimeError("No wifi connection found - must be connected to wifi first")

        packet = bytearray(48)
        packet[0] = 0b00100011

        for i in range(1, len(packet)):
            packet[i] = 0

        if pool is None:
            print("WARNING: No socket pool supplied to get_time. Creating one")
            pool = socketpool.SocketPool(wifi.radio)

        count = 0
        while True:

            if count > 5:
                print(f"WARNING: Haven't been able to set NTP after {count} attempts")

            try:
                with pool.socket(pool.AF_INET, pool.SOCK_DGRAM) as sock:
                    sock.settimeout(1)  # Ensure we don't block forever
                    sock.sendto(packet, (cls.NTP_SERVER, cls.NTP_PORT))
                    sock.recv_into(packet)
                    destination = time.monotonic_ns()

                    seconds = struct.unpack_from("!I", packet, offset=len(packet) - 8)[0]
                    return seconds - cls.NTP_TIME_CORRECTION - (destination // 1_000_000_000)

            except OSError:
                count += 1
                pass

    def update_rtc(self, pool=None):
        """ Sets the system RTC to actually be what NTP sends us back
        """
        utc = self.get_time(pool=pool)
        tz_offset = int(self.TZ_OFFSET * 3600)  # Seconds of our TZ offset, round down
        now = time.localtime(time.monotonic_ns() // 1_000_000_000 + utc + tz_offset)
        rtc.RTC().datetime = now  # Actually set the clock now

    @classmethod
    def timestamp(cls):
        d = rtc.RTC().datetime
        return f"{d.tm_year}/{d.tm_mon:02}/{d.tm_mday:02} {d.tm_hour:02}:{d.tm_min:02}:{d.tm_sec:02}"

    @classmethod
    def iso_timestamp(cls):
        d = rtc.RTC().datetime
        return f"{d.tm_year}/{d.tm_mon:02}/{d.tm_mday:02}T{d.tm_hour:02}:{d.tm_min:02}:{d.tm_sec:02}.000000"
